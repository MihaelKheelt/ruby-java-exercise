package com.retriever.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by czumelzu on 27-09-17.
 */

public class Item {
    private Integer monitors;
    private Integer checkRate;
    private Integer history;
    private Boolean multipleNotifications;
    private Boolean pushNotifications;
    private Double price;

    public Integer getMonitors() {
        return monitors;
    }

    public void setMonitors(Integer monitors) {
        this.monitors = monitors;
    }
    @JsonProperty("check-rate")
    public Integer getCheckRate() {
        return checkRate;
    }

    public void setCheckRate(Integer checkRate) {
        this.checkRate = checkRate;
    }

    public Integer getHistory() {
        return history;
    }

    public void setHistory(Integer history) {
        this.history = history;
    }

    @JsonProperty("multiple-notifications")
    public Boolean getMultipleNotifications() {
        return multipleNotifications;
    }

    public void setMultipleNotifications(Boolean multipleNotifications) {
        this.multipleNotifications = multipleNotifications;
    }

    @JsonProperty("push-notifications")
    public Boolean getPushNotifications() {
        return pushNotifications;
    }

    public void setPushNotifications(Boolean pushNotifications) {
        this.pushNotifications = pushNotifications;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
