package com.retriever.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by czumelzu on 28-09-17.
 */
@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Generic Server Problem")
public class ApplicationException extends Exception{
        public ApplicationException() {}

        public ApplicationException(String message)
        {
            super(message);
        }
}
