package com.retriever.controller;

import com.retriever.domain.Item;
import com.retriever.exception.ApplicationException;
import com.retriever.service.HtmlParseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by czumelzu on 27-09-17.
 */
@RestController
public class MainController {

    @Autowired
    private HtmlParseService htmlParseService;

    @RequestMapping(value="/prices", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getPrices() throws ApplicationException {
        return htmlParseService.getItemsFromPage();
    }
}
