package com.retriever.service;

import com.retriever.domain.Item;
import com.retriever.exception.ApplicationException;

import java.util.List;

/**
 * Created by czumelzu on 27-09-17.
 */
public interface HtmlParseService {
    List<Item> getItemsFromPage() throws ApplicationException;
}

