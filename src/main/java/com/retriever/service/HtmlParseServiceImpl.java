package com.retriever.service;

import com.retriever.domain.Item;
import com.retriever.exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by czumelzu on 27-09-17.
 */
@Service
public class HtmlParseServiceImpl implements HtmlParseService{

    @Autowired
    private CallRemotePage callRemotePage;

    public List<Item> getItemsFromPage() throws ApplicationException {
        List<Item> response = new ArrayList<>();
        try {
            response = callRemotePage.getHtmlDocument();
        } catch (Exception e){
            throw new ApplicationException("Problema generico ");
        }
        return response;
    }
}
