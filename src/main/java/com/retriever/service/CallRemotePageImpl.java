package com.retriever.service;

import com.retriever.domain.Item;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.retriever.util.UtilMap.*;

/**
 * Created by czumelzu on 28-09-17.
 */
@Service
public class CallRemotePageImpl implements CallRemotePage {
    private static final String URL_ENDPOINT_SERVICE_HTML = "https://www.port-monitor.com/plans-and-pricing";

    public List<Item> getHtmlDocument() throws IOException {
        List<Item> itemList = new ArrayList<>();
        Document doc = Jsoup.connect(URL_ENDPOINT_SERVICE_HTML).get();
        return mapResponseFromHtml(doc);
    }

    private List<Item> mapResponseFromHtml(Document doc) {
        List<Item> itemList = new ArrayList<>();
        Element content = createElementById(doc, "main");
        Elements products = createElementByClass(content, "product");
        for (Element product : products) {
            Integer monitors = createMonitors(product, "h2");
            Elements thinContent = createElementByClass(product, "thin");
            if (thinContent != null) {
                Element thinContainer = thinContent.first();
                Integer checkRate = createCheckRate(thinContainer, "dd");
                Integer history = createHistory(thinContainer, "dd");
                Boolean multipleNotifications = createMultipleNotifications(thinContainer, "dd");
                Boolean pushNotifications = createPushNotifications(thinContainer, "dd");
                Double price = createPrice(product, "btn-success");

                Item item = new Item();
                item.setCheckRate(checkRate);
                item.setHistory(history);
                item.setMonitors(monitors);
                item.setMultipleNotifications(multipleNotifications);
                item.setPushNotifications(pushNotifications);
                item.setPrice(price);
                itemList.add(item);
            }
        }
        return itemList;

    }
}
