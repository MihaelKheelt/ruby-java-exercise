package com.retriever.service;

import com.retriever.domain.Item;

import java.io.IOException;
import java.util.List;

/**
 * Created by czumelzu on 28-09-17.
 */
public interface CallRemotePage {
    List<Item> getHtmlDocument() throws IOException;
}
