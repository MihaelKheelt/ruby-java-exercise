package com.retriever.util;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

/**
 * Created by mclovin on 28-09-17.
 */
public class UtilMap {

    public static Integer createMonitors(Element elem, String val) {
        Elements result = createElementByTag(elem, val);
        if (result != null && isNumeric(result.text())) {
            return Integer.parseInt(result.text());
        }
        return null;
    }

    public static Double createPrice(Element elem, String val) {
        Elements mainElement = createElementByClass(elem, val);
        if (mainElement != null && mainElement.first() != null && isNotBlank(mainElement.first().text())) {
            return Double.parseDouble(mainElement.first().text().split(" ")[0].substring(1));
        }
        return null;
    }

    public static Boolean createPushNotifications(Element elem, String val) {
        Elements mainElement = createElementByTag(elem, val);
        if (mainElement != null && mainElement.size() > 3) {
            Elements elements = createElementByTag(mainElement.get(3), "span");
            if (elements != null && elements.first() != null && elements.first().text() != null) {
                return elements.first().text().equalsIgnoreCase("yes");
            }
        }
        return null;
    }

    public static Boolean createMultipleNotifications(Element elem, String val) {
        Elements mainElement = createElementByTag(elem, val);
        if (mainElement != null && mainElement.size() > 2) {
            Elements elements = createElementByTag(mainElement.get(2), "span");
            if (elements != null && elements.first() != null && elements.first().text() != null) {
                return elements.first().text().equalsIgnoreCase("yes");
            }
        }
        return null;
    }

    public static Integer createHistory(Element elem, String val) {
        Elements mainElement = createElementByTag(elem, val);
        if (mainElement != null && mainElement.size() > 1) {
            String result = mainElement.get(1).text();
            result = isNotBlank(result) ? result.split(" ")[0] : "0";
            return Integer.parseInt(result);
        }
        return null;
    }

    public static Integer createCheckRate(Element elem, String val) {
        Elements mainElement = createElementByClass(elem, val);
        if (mainElement != null && mainElement.size() > 0) {
            String result = mainElement.get(0).text();
            result = isNotBlank(result) ? result.split(" ")[1] : "0";
            return Integer.parseInt(result);
        }
        return null;
    }

    public static Element createElementById(Document input, String id) {
        Element result = null;
        if (input != null && isNotBlank(id))
            result = input.getElementById(id);
        return result;
    }

    public static Element createElementById(Element elem, String id) {
        Element result = null;
        if (elem != null && isNotBlank(id))
            result = elem.getElementById(id);
        return result;
    }

    public static Elements createElementByClass(Element elem, String classText) {
        Elements result = null;
        if (elem != null && isNotBlank(classText))
            result = elem.getElementsByClass(classText);
        return result;
    }

    private static Elements createElementByTag(Element elem, String classText) {
        Elements result = null;
        if (elem != null && isNotBlank(classText))
            result = elem.getElementsByTag(classText);
        return result;
    }
}
