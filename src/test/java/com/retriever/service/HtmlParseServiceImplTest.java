package com.retriever.service;

import com.retriever.domain.Item;
import com.retriever.exception.ApplicationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * Created by czumelzu on 28-09-17.
 */
@RunWith(MockitoJUnitRunner.class)
public class HtmlParseServiceImplTest {

    @Mock
    private CallRemotePage callRemotePage;

    @InjectMocks
    private HtmlParseServiceImpl htmlParseService;

    @Test
    public void getItemsFromPage() throws ApplicationException, IOException {
        when(callRemotePage.getHtmlDocument()).thenReturn(new ArrayList<>());
        List<Item> listItems =htmlParseService.getItemsFromPage();
        assertNotNull(listItems);
        assertEquals(0,listItems.size());
    }

    @Test
    public void getItemsFromPageWithNullInput() throws ApplicationException, IOException {
        when(callRemotePage.getHtmlDocument()).thenReturn(null);
        List<Item> listItems =htmlParseService.getItemsFromPage();
        assertNull(listItems);
    }
    @Test(expected = ApplicationException.class)
    public void getItemsFromPageWithIOException() throws ApplicationException, IOException {
        when(callRemotePage.getHtmlDocument()).thenThrow(new IOException ("dummy"));
        htmlParseService.getItemsFromPage();
    }
    @Test(expected = ApplicationException.class)
    public void getItemsFromPageWithException() throws ApplicationException, IOException {
        when(callRemotePage.getHtmlDocument()).thenThrow(new IndexOutOfBoundsException ("dummy"));
        htmlParseService.getItemsFromPage();
    }
}
