package com.retriever.controller;

import com.retriever.TestContext;
import com.retriever.domain.Item;
import com.retriever.exception.ApplicationException;
import com.retriever.service.HtmlParseService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by czumelzu on 28-09-17.
 */
@WebAppConfiguration
@ContextConfiguration(classes = {TestContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class MainControllerTest {
    private MockMvc mockMvc;

    @Mock
    private HtmlParseService htmlParseService;

    @InjectMocks
    private MainController mainController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(mainController)
                .build();
    }
    @Test
    public void getPricesWithException() throws Exception {
        List<Item> users = new ArrayList<>();
        when(htmlParseService.getItemsFromPage()).thenThrow(new ApplicationException("Error"));
        mockMvc.perform(get("/prices"))
                .andExpect(status().isInternalServerError());

        verify(htmlParseService, times(1)).getItemsFromPage();
        verifyNoMoreInteractions(htmlParseService);
    }
}
